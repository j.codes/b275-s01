<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// to access with the authenticated user
use Illuminate\Support\Facades\Auth;
// To have access with queries related to the Post Entity/Model.
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing a form for post creation
    public function create(){
        if(Auth::user()){
            return view('posts.create');  
        }
        else{
            return redirect('/login');
        }       
    }
    
    // action to receive a form data and subsequently store said data in the post table
    public function store(Request $request){
        // if there is an authenticated user
        if(Auth::user()){
            // instantiate a new post object from the Post model
            $post = new Post;
            // define the properties of the $post object using the receive form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            // save this post object in the database
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all the blog posts
    public function index(){
        // this is similar with DB::table('Post')->get();
        $posts = Post::where('isActive', 1)->get();
        // The "with()" method will allow us to pass information from the controller to view page
        return view('posts.index')->with("posts", $posts);
    }

    // Activity s02
    public function welcome(){
        $posts = Post::where('isActive', 1)
                ->inRandomOrder()
                ->limit(3)
                ->get();
        return view('welcome')->with('posts', $posts);
    }


    // action to show only the posts authored by the authenticated user.
    public function myPost(){
        if(Auth::user()){
            // We are able to fetch the posts related to a specific user because of the established relationship between the models.
            $posts = Auth::user()->posts;

            return view('posts.index')->with("posts", $posts);
        }
        else
        {
            return redirect('/login');
        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);
        $comments = PostComment::where('post_id', $id)->get();
        $likes = PostLike::where('post_id', $id)->get();

        return view('posts.show')->with('post', $post)->with('comments', $comments)->with('likes', $likes);
    }

    // Activity s03
    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);

            if(Auth::user()->id == $post->user_id){
                return view('posts.edit')->with('post', $post);
            }
            else
            {
                return redirect('/posts');
            }   
        }
        else
        {
            return redirect('/login');
        }
    }

    public function update(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action to remove a post with the matching URL id parameter.
    public function destroy($id){
        $post = Post::find($id);

         if(Auth::user()->id == $post->user_id){
            $post->delete();
         }
         return redirect('/posts');
    }

    // Activity s04
    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = 0;
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will allow the authenticated user who is not the post author to toggle a like/unlike on the post being viewed.
    public function likes($id){
        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;

            // if the authenticated user is not the post owner
            if($post->user_id != $user_id){
                // check is a post like has been made
                if($post->likes->contains("user_id", $user_id)){
                    // delete the like made by the user to unlike this post.
                    PostLike::where('post_id', $post->id)
                    ->where('user_id', $user_id)
                    ->delete();
                }
                // create a new like record in the post_likes table with the user_id and post_id
                else{
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;
                    $postLike->save();
                }
            }

            return redirect("/posts/$id");

        }
        else{
            return redirect('/login');
        }
    }

    // Activity s05
    public function comments(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;

            $postComment = new PostComment;
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->content = $request->input('comment-text');
            $postComment->save();

            return redirect("/posts/$id");

        }
        else{
            return redirect('/login');
        }
    }

}