{{-- Activity for s02 --}}

@extends('layouts.app')

@section('content')

	<div class="text-center">
		<img src="{{url('/images/laravel-logo.png')}}" alt="Image" class="img-fluid w-50"/>
	</div>
	{{-- If there are already a post created it will be displayed on our views --}}

	<div class="text-center my-4">
		<h2>Featured Posts:</h2>
	</div>

	@if(count($posts) > 0)
		@foreach ($posts as $post)
			<div class="card text-center my-2">
				<div class="card-body">
					<h4 class="card-title mb-3">
						<a href="/posts/{{$post->id}}">
							{{$post->title}}
						</a>
					</h4>
					<h6 class="card-text mb-3">
						Author: {{$post->user->name}}
					</h6>
				</div>
			</div>
		@endforeach
	@else
		<div>
			<h2>There are no post to show.</h2>
		</div>
	@endif
@endsection