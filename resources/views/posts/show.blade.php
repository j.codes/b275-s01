@extends('layouts.app')

@section('content')
	<?php 
		$totalLikes = count($likes);
		$totalComments = count($comments); 
	?>

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			<p class="card-subtitle text-muted mb-2">Likes: {{$totalLikes}} | Comments: {{$totalComments}}</p>

			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif

						<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
						  Comment
						</button>
					</form>
				@else
					<form class="d-inline">
						@csrf

						<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
						  Comment
						</button>
					</form>
				@endif
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	{{-- Comment Section for s05 Activity --}}
	<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="commentModalLabel">Leave a comment</h5>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>

      		<form method="POST" action="/posts/{{$post->id}}/comment">
      		@method('GET')
	        @csrf
		      <div class="modal-body">
	          <div class="mb-3">
	            <label for="comment-text" class="col-form-label">Content:</label>
	            <textarea class="form-control" id="comment-text" name="comment-text"></textarea>
	          </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">Post Comment</button>
		      </div>
		    </form>
	    </div>
	  </div>
	</div>

	{{-- Modal for s05 Activity (Posting comments) --}}
	@if(count($comments) > 0)
		@foreach($comments as $comment)
			<div class="card text-center my-2">
				<div class="card-body">
					<h5 class="card-text mb-3">
						{{$comment->content}}
					</h5>
					<h6 class="card-text mb-3">
						Posted by: {{$comment->user->name}}
					</h6>
					<p class="card-subtitle mb-3 text-muted">Created at: {{$comment->created_at}}</p>
				</div>
			</div>
		@endforeach
	@else
		<div class="py-4">
			<h5>There are no comments to show.</h5>
		</div>
	@endif
@endsection
